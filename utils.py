from gestalt.web.utils       import *

from uchikoma.connector.utils       import *
from uchikoma.connector.models      import *
from uchikoma.connector.schemas     import *

################################################################################

DYNO_STATEs = (
    ('sleepy', "Sleepy & Inactive"),
    ('active', "Active & Alive"),
    ('stale',  "Inexistant & Deleted"),
)

DYNO_REPOs = (
    ('github',    "GitHub Inc"),
    ('bitbucket', "Atlassian BitBucket"),
    ('gitlab',    "GitLab Community"),
)

DYNO_PLATFORMs = (
    ('unknown',   "Unknown"),
    ('heroku',    "Heroku"),
    ('openshift', "OpenShift"),
)

