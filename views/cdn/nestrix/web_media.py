#-*- coding: utf-8 -*-

from demantia.contrib.cloud.shortcuts import *

################################################################################

@fqdn.route(r'^nestrix/media/$', strategy='login')
def listing(context):
    context.template = 'cloud/views/nestrix/media/list.html'

    return {'version': version}

#*******************************************************************************

@fqdn.route(r'^nestrix/media/choose$', strategy='login')
def chooser(context):
    context.template = 'cloud/views/nestrix/media/select.html'

    return {'version': version}

#*******************************************************************************

@fqdn.route(r'^nestrix/media/upload$', strategy='login')
def uploader(context):
    context.template = 'cloud/views/nestrix/media/upload.html'

    return {'version': version}

################################################################################

@fqdn.route(r'^nestrix/media/(?P<provider>.+)/(?P<owner>.+)/(?P<slug>.+)$', strategy='login')
def overview(context, provider, owner, slug):
    context.template = 'cloud/views/nestrix/media/view.html'

    return {'version': version}
