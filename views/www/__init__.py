#-*- coding: utf-8 -*-

from uchikoma.entertain.shortcuts import *

################################################################################

# @Reactor.router.register_route('connect', r'^$', strategy='login')
@render_to('www/views/homepage.html')
def homepage(request):
    return context(
        primitives=enumerate_schemas(),
    )

