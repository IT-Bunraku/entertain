#-*- coding: utf-8 -*-

from deming.contrib.dev.shortcuts import *

################################################################################

@fqdn.route(r'^languages/?$', strategy='login')
def listing(context):
    context.template = 'dev/rest/repo/list.html'

    return {
        'listing': ScmRepository.objects(owner='lang2use', slug__in='2use'),
    }

#*******************************************************************************

@fqdn.route(r'^languages/(?P<narrow>.+)/?$', strategy='login')
def overview(context, narrow):
    context.template = 'dev/rest/repo/list.html'

    return {
        'repo': ScmRepository.get(owner='lang2use', slug=narrow+'2use'),
    }
