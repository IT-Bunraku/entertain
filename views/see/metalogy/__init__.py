#-*- coding: utf-8 -*-

from kungfu.contrib.ops.shortcuts import *

#*******************************************************************************

from . import operatingsystem
from . import distributions

from . import serving
from . import platforms

################################################################################
