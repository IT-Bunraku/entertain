#-*- coding: utf-8 -*-

from kungfu.contrib.ops.shortcuts import *

################################################################################

@fqdn.route(r'^~(?P<org>.+)/vhosts/$', strategy='login')
def listing(context, org):
    context.template = 'ops/rest/vhost/list.html'

    return {
        'listing': [
            app
            for app in HerokuAPI.apps
        ] + [
            #app
            #for status,app in in RedHatCloud.app_list_all()
        ],
    }

#*******************************************************************************

@fqdn.route(r'^~(?P<org>.+)/vhosts/(?P<provider>.+)/(?P<slug>.+)$', strategy='login')
def overview(context, org, provider, slug):
    context.template = 'ops/rest/vhost/view.html'

    if provider=='heroku':
        res = HerokuAPI.apps[slug]
        
        return {
            'vhost': res,
        }
    elif provider=='openshift':
        qs = RedHatCloud.app_list_all()
        
        res = qs[slug]
        
        return {
            'vhost': res,
        }

