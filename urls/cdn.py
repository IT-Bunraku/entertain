from django.conf.urls import include, url

from gestalt.web.helpers import Reactor

from uchikoma.devops.views import dev as Views

################################################################################

urlpatterns = Reactor.router.urlpatterns('cdn',
    #url(r'^repos/$',                      Views.repo__list, name='repo__list'),
    #url(r'^repos/(?P<narrow>[^/]+)/$',    Views.repo__view, name='repo__view'),

    url(r'^$',                            Views.Homepage.as_view(), name='homepage'),
)
