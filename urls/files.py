from django.conf.urls import include, url

from gestalt.web.helpers import Reactor

from uchikoma.devops.views import cloud as Views

################################################################################

urlpatterns = Reactor.router.urlpatterns('files',
    #url(r'^@(?P<owner>[^/]+)/repos$',                Prime.repo__list, name='repo__list'),
    #url(r'^@(?P<owner>[^/]+)/dynos$',                Prime.dyno__list, name='dyno__list'),
    #url(r'^@(?P<owner>[^/]+)/backends$',             Prime.backend__list, name='backend__list'),
    #url(r'^@(?P<owner>[^/]+)/~(?P<narrow>[^/]+)/$',  Prime.repo__view, name='repo__view'),
    #url(r'^@(?P<owner>[^/]+)/-(?P<narrow>[^/]+)/$',  Prime.dyno__view, name='dyno__view'),
    #url(r'^@(?P<owner>[^/]+)/\+(?P<narrow>[^/]+)/$', Prime.backend__view, name='backend__view'),

    #url(r'^backends/$',                   Views.backend__list, name='backend__list'),
    #url(r'^backends/(?P<narrow>[^/]+)/$', Views.backend__view, name='backend__view'),

    url(r'^$',                               Views.Homepage.as_view(), name='homepage'),
)
