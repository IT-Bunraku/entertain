#-*- coding: utf-8 -*-

from gestalt.core.helpers import *

###################################################################################

@extend_reactor('media')
class Entertain_Media(ReactorExt):
    def extract(self):
        pass

    #***************************************************************************

    def classify(self):
        pass

###################################################################################

from . import Text
from . import Image
from . import Video

